Creative Portion:
* Anonymous Sign In
	* I always appreciate it when games don't make you sign in to use them so I wanted to enable anonymous users
* More user details if you click on the leaderboard
	* Clickable Listview starts a new activity that shows more details about the user
	* Ideally it wouldn't be based on email because that really exposes a user but I didn't know how to use Firebase so that usernames/display names didn't conflict. 
	* Now you can see if the person who ranks first is a 12 year old kid and how many losses they've had, etc. 

(10 points) Player can login and login data is stored in Firebase
(10 points) Player’s win/loss counts are displayed on startup
(10 points) The Player receives two cards face up, and the dealer receives one card face up and one card face down
(2 points) Swiping right allows the Player to hit
	-3 Your game only allows the player to hit once...that's not how Blackjack is played
(5 points) Double tapping allows the Player to stand
(10 points) Cards being dealt are animated, and all cards are visible.
(5 points) If the Player goes over 21, they automatically bust
(10 points) The dealer behaves appropriately based on the rules described above
(10 points) Once the game is complete, the winner should be declared and the Firebase database should be updated appropriately
(5 points) A new round is automatically started after each round
	-2 The screen goes black between games which is not a smooth transition. I thought the app had crashed for a moment
(5 points) Player can logout
(10 points) A leaderboard is shown in a separate tab or activity and is consistent among installations of the app
(15 points) Creative portion!

I like the player details you incorporated for your creative portion. Great job!

Total: 105 / 100