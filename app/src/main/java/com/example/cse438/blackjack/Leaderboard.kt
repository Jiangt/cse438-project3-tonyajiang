package com.example.cse438.blackjack

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_leaderboard.*

class Leaderboard : AppCompatActivity() {
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leaderboard)

        // Map from email to user details
        val allData = mutableMapOf<String, UserDetails?>()
        // List of users by win-loss ratio
        val lb = ArrayList<String>()

        // Get every document in the collection
        db.collection("data")
            .get()
            .addOnSuccessListener { result ->
                val map = mutableMapOf<String, Double?>()
                // Get every user
                // Keep track of their ratio and put it in a map
                // Save the entire user to another map
                for (document in result) {
                    val name = document.getString("name")
                    if (name != null) {
                        val win = document.getDouble("win")
                        val loss = document.getDouble("loss")
                        val description = document.getString("description")
                        val home = document.getString("home")
                        val age = document.getDouble("age")
                        val u = UserDetails(name, win, loss, description, home, age)
                        var ratio = 0.0
                        if (win != null && loss != null) {
                            ratio = when {
                                win == 0.0 -> 0.0
                                loss == 0.0 -> 100.0
                                else -> win / loss
                            }
                        }
                        map[name] = ratio
                        allData[name] = u
                    }
                }

                // Sort the map with win-loss ratio by descending value
                val list = map.toList().sortedByDescending { (_, value) -> value }

                // Now that it's in order, add just the names to a list
                for (pair in list) {
                    lb.add(pair.first)
                }
                val adapter = ArrayAdapter(
                    this,
                    android.R.layout.simple_list_item_1, lb
                )
                listview.adapter = adapter
            }
            .addOnFailureListener { exception ->
                Log.d("LEADERBOARD", "Error getting documents: ", exception)
            }



        listview.setOnItemClickListener { _, _, position, _ ->
            val email = lb[position]
            val user = allData[email]

            val intent = Intent(this, Details::class.java)
            intent.putExtra("USER", user)
            startActivity(intent)
        }
    }
}
