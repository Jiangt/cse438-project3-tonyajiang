package com.example.cse438.blackjack

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details.*

class Details : AppCompatActivity() {

    private lateinit var user: UserDetails

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        // Get the relevant user from the list item
        user = intent.extras?.getSerializable("USER") as UserDetails
        this.loadUI(user)
    }

    private fun loadUI(user: UserDetails) {
        name.text = user.name
        desc.text = user.description
        age.text = user.age.toString()
        home.text = user.home
        win.text = user.win.toString()
        loss.text = user.loss.toString()
    }

    override fun onBackPressed() {
        this.finish()
    }
}
