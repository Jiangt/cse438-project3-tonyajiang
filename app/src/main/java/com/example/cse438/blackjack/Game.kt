package com.example.cse438.blackjack

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.GestureDetectorCompat
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import com.example.cse438.blackjack.util.CardRandomizer
import kotlinx.android.synthetic.main.activity_game.*
import java.util.*
import android.widget.ImageView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Game : AppCompatActivity() {
    private lateinit var mDetector: GestureDetectorCompat
    private var height: Int = 0
    private var width: Int = 0
    private var isGameOver = false
    private var playerScore = 0
    private var dealerScore = 0
    private var playersTurn = true
    private var playersHandWidth = 0f
    private var dealersHandWidth = 0f
    private lateinit var randomizer : CardRandomizer
    private lateinit var cardList: ArrayList<Int>

    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseFirestore.getInstance()
    private val user = auth.currentUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        // Set button listeners
        logout_btn.setOnClickListener {
            auth.signOut()
            finish()
        }

        leaderboard_btn.setOnClickListener {
            val intent = Intent(this, Leaderboard::class.java)
            startActivity(intent)
        }

        // Set gesture listener
        mDetector = GestureDetectorCompat(this, MyGestureListener())

        // Get Screen Dimensions
        val metrics = this.resources.displayMetrics
        this.height = metrics.heightPixels
        this.width = metrics.widthPixels

        // Initialize Card Picker
        randomizer = CardRandomizer()
        cardList = randomizer.getIDs(this)

        // If the user has an account, show their ratio
        if (user != null && !user.isAnonymous) {
            getPlayersRatio()
        }

        // deal the cards, wait for the cards to be dealt and then check scores for winners
        dealCards()
        val handler = Handler()
        handler.postDelayed({
            postDealPredicate()
        }, 1500)
    }

    override fun onTouchEvent(event: MotionEvent) : Boolean {
        mDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    private fun getPlayersRatio(){
        // Pull win and loss numbers from firestore
        val docRef = db.collection("data").document(user?.uid.toString())
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val win = document.getDouble("win")?.toInt()
                    val loss = document.getDouble("loss")?.toInt()
                    ratio_text.text = "Player's Win/Loss Count: $win: $loss"
                } else {
                    Log.d("PLAYER RATIO", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("PLAYER RATIO", "get failed with ", exception)
            }
    }

    private fun postDealPredicate() {
        // Check for winners right after dealing the cards
        // End the game immediately if someone has won
        // Otherwise it's the players turn
        if (playerScore == 21){
            turn_indicator.text = "Player Won"
            gameOver(true, true)
            return
        }
        if (dealerScore == 21) {
            turn_indicator.text = "Dealer Won"
            gameOver(true, false)
            return
        }
        turn_indicator.text = "Player's Turn"
    }

    private fun dealCards(){
        // Deal 4 cards face down and then flip them up and add their values to their respective scores
        val rand = Random()

        animateCard(this.width/2f,this.height/2f - 50f, playerCard1)
        animateCard(this.width/2f - 100,this.height/2f - 50f, playerCard2)
        animateCard(this.width/2f, 0f, dealerCard1)
        animateCard(this.width/2f - 100, 0f, dealerCard2)
        playersHandWidth = this.width/2f - 100
        dealersHandWidth = this.width/2f - 100

        playerCard1.postDelayed(
            {
                val id = cardList.get(rand.nextInt(cardList.size))
                val name = resources.getResourceEntryName(id)
                playerScore += parseDealtCard(name)
                playerCard1.setImageDrawable(resources.getDrawable(id, null))

            }, 1000)

        playerCard2.postDelayed(
            {
                val id = cardList.get(rand.nextInt(cardList.size))
                val name = resources.getResourceEntryName(id)
                playerScore += parseDealtCard(name)
                playerCard2.setImageDrawable(resources.getDrawable(id, null))
            }, 1000)

        dealerCard2.postDelayed(
            {
                val id = cardList.get(rand.nextInt(cardList.size))
                val name = resources.getResourceEntryName(id)
                dealerScore += parseDealtCard(name)
                dealerCard2.setImageDrawable(resources.getDrawable(id, null))
            }, 1000)

        // This is the dealer's hidden card
        val id = cardList.get(rand.nextInt(cardList.size))
        val name = resources.getResourceEntryName(id)
        dealerScore += parseDealtCard(name)
    }

    private fun animateCard(targetX: Float, targetY: Float, card: View){
        val animSetXY = AnimatorSet()

        val x = ObjectAnimator.ofFloat(
            card,
            "translationX",
            card.translationX,
            targetX
        )

        val y = ObjectAnimator.ofFloat(
            card,
            "translationY",
            card.translationY,
            targetY
        )

        animSetXY.playTogether(x, y)
        animSetXY.duration = 1000
        animSetXY.start()
    }

    private fun parseDealtCard(name: String): Int{
        // Get the value of a card by parsing its name with regex
        // Follows Blackjack rules
        var num = 0
        if ("ace".toRegex().containsMatchIn(name)){
            num = if(playersTurn) {
                    if (playerScore > 10) {
                        1
                    } else {
                        11
                    }
                } else {
                    if (dealerScore > 10) {
                        1
                    } else {
                        11
                    }
                }
        } else if ("(jack|king|queen)".toRegex().containsMatchIn(name)){
            num = 10
        } else if ("\\d".toRegex().containsMatchIn(name)){
            val cardVal = "\\d+".toRegex().find(name)
            if(cardVal != null){
                num = cardVal.value.toInt()
            }
        }
        return num
    }

    private fun winPredicate(){
        // Logic for different scenarios
        // If someone bust or own, the game is over
        // Otherwise, it's the other players turn
        var playerDidWin = false
        if(playersTurn){
            if(playerScore > 21){
                turn_indicator.text = "Player Bust"
                isGameOver = true
            } else if (playerScore == 21){
                turn_indicator.text = "Player Won"
                playerDidWin = true
                isGameOver = true
            } else {
                turn_indicator.text = "Dealer's Turn"
            }
        } else {
            if(dealerScore > 21){
                turn_indicator.text = "Dealer Bust"
                playerDidWin = true
                isGameOver = true
            } else if (dealerScore == 21){
                turn_indicator.text = "Dealer Won"
                isGameOver = true
            } else if(dealerScore > 17){
                if(playerScore >= dealerScore){
                    turn_indicator.text = "Player Won"
                    playerDidWin = true
                } else {
                    turn_indicator.text = "Dealer Won"
                }
                isGameOver = true
            } else {
                turn_indicator.text = "Player's Turn"
                playersTurn = true
            }
        }

        // store players win-lose ratio in the database
        gameOver(isGameOver, playerDidWin)
    }

    private fun gameOver(isGameOver: Boolean, playerDidWin: Boolean){
        // update the database depending on who won and restart a new game in 3 seconds
        playersTurn = false
        if (isGameOver){
            if(playerDidWin){
                updateDb(true)
            } else {
                updateDb(false)
            }
            root.postDelayed({recreate()}, 3000)
        }
    }

    private fun updateDb(didWin: Boolean){
        // If the player won, add one to their win field
        // Otherwise, add one to their loss field
        // Update Firestore
        val docRef = db.collection("data").document(user?.uid.toString())
        db.runTransaction { transaction ->
            val snapshot = transaction.get(docRef)
            var field = if(didWin) "win" else "loss"
            val change = snapshot.getDouble(field)!! + 1
            transaction.update(docRef, field, change)

            // Success
            null
        }.addOnSuccessListener { Log.d("FIRESTORE", "Transaction success!") }
            .addOnFailureListener { e -> Log.w("FIRESTORE", "Transaction failure.", e) }
    }

    private fun hit() {
        // Deal a new card by getting a random new card
        // Animate it and add the score to either the player or dealer
        val deck = ImageView(this)
        root.addView(deck)

        val rand = Random()
        val id = cardList.get(rand.nextInt(cardList.size))
        val name = resources.getResourceEntryName(id)
        deck.setImageDrawable(resources.getDrawable(id, null))

        if(playersTurn){
            // update the location for where cards to need animate
            playersHandWidth -= 100
            //animate
            animateCard(playersHandWidth,this.height/2f  - 50f, deck)
            // update score
            playerScore += parseDealtCard(name)
        } else {
            dealersHandWidth -= 100
            animateCard(dealersHandWidth,0f, deck)
            dealerScore += parseDealtCard(name)
        }

        winPredicate()
    }

    private fun dealersTurn(){
        // Dealer plays by specific rules
        // I delay the dealers play so it looks more natural
        // Otherwise, it looks like the player and dealer play at the same time
        if(isGameOver){
            return
        }
        turn_indicator.text = "Dealer's Turn"
        playersTurn = false
        val handler = Handler()
        handler.postDelayed({
            when {
                dealerScore == 21   -> turn_indicator.text = "Dealer Won"
                dealerScore > 17    -> winPredicate()
                else                -> hit()
            }
            playersTurn()
        }, 500)

    }

    private fun playersTurn(){
        // Make sure the game isn't over,
        // update relevant variables
        // wait for user to make move
        if(isGameOver){
            return
        }
        turn_indicator.text = "Player's Turn"
        playersTurn = true
    }

    private inner class MyGestureListener : GestureDetector.SimpleOnGestureListener() {

        private var swipedistance = 150

        override fun onDoubleTap(e: MotionEvent?): Boolean {
            // Disable tap if its not the players turn
            // If stand - its dealers turn
            if(playersTurn){
                dealersTurn()
                return true
            }
            return false
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            // Disable swipe if its not the players turn
            // If hit, call hit method and then its the dealers turn
            if(playersTurn){
                if (e2.x - e1.x > swipedistance) { // right
                    hit()
                    dealersTurn()
                    return true
                }
            }
            return false
        }
    }
}
