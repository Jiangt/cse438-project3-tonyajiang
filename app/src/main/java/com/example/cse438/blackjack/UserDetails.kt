package com.example.cse438.blackjack
import java.io.Serializable

class UserDetails() :  Serializable {
    var name: String = ""
    var win: Double? = 0.0
    var loss: Double? = 0.0
    var description: String? = ""
    var home: String? = ""
    var age: Double? = 0.0

    constructor(
        name: String,
        win: Double?,
        loss: Double?,
        description: String?,
        home: String?,
        age: Double?
    ): this() {
        this.name = name
        this.win = win
        this.loss = loss
        this.description = description
        this.home = home
        this.age = age
    }
}